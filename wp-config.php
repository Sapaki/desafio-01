<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'Desafio-01' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

if ( !defined('WP_CLI') ) {
    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
    define( 'WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
}



/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'bT2MBlo8eIbcXl4X5Ih4rV75xF6wSU6FnDsILiyZZM7feZxd4rx2KwuVU9oQKZxd' );
define( 'SECURE_AUTH_KEY',  'vjsF3vNC7EP9JlqhUwBSWrVi7dVuw8j33OXZchSQIUNUZ9LNwI07xd1aNyoxCHiP' );
define( 'LOGGED_IN_KEY',    'aiI666uHRNezkKTvhMiVnCox0TniE6wa1Oaqlx0bowyz1eASj09dbi5XwTnD5fWe' );
define( 'NONCE_KEY',        'NsDqJ2lAyifpaW87cDrBFUhW4r4wkP4Azjcl5Reo5MNFByxP0JuRV1qo1NKWQ4c1' );
define( 'AUTH_SALT',        'w7AYAUJ6pLr3iN4ztIfooPVUro5umcBbngPO11TXK7Btsb89ORMVoORTe1YBF3CF' );
define( 'SECURE_AUTH_SALT', 'm5TO2muqtQd3wvVwyqhFHfViuTdMcmOFPaglvTk2rRtzXRcVdoETv53OrWGrfUAQ' );
define( 'LOGGED_IN_SALT',   'jweRFMLpHU0Q4TumYlX3shHf4OHNp02lwwkqcBlq9HVnJQU6rtuhmpiNnafESXIU' );
define( 'NONCE_SALT',       'tMXp6IXQjyQOYNHzy8v1erAcMFDSMlU67hO8ETHHR22cpaK5euR1Bimf3q0oj4ym' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
